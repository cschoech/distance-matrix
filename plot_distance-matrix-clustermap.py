"""
Script to visualize a distance matrix as a cluster heatmap using seaborn. 
https://seaborn.pydata.org/generated/seaborn.clustermap.html
"""


# === Imports === 

import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt
from os.path import join
import os
import itertools
import numpy as np


# === Functions === 

def make_clustermap(matrixfile): 
    workdir = join(os.path.realpath(os.path.dirname(__file__)))
    clustermapfile = join(workdir, "distance-matrix-clustermap.png")
    matrixfile = join(workdir, matrixfile)
    with open(matrixfile, "r", encoding="utf8") as infile: 
        matrix = pd.read_csv(infile, sep=" ", index_col=0)
    sns.set_style("whitegrid")
    plt.figure()
    plt.title("Cluster heatmap of stylometric distance matrix")
    sns.clustermap(
        data=matrix,
        method="ward", # ward|centroid etc.
        robust=True,
        figsize=(18,18),
        annot=True,
        square=False,
        linewidths=0.8,
        linecolor="white",
        cmap="hot",
        cbar=True,    
        )
    plt.tight_layout()
    plt.savefig(clustermapfile, dpi=300)
    plt.close()

make_clustermap(matrixfile="distance_table_1500mfw_0c.txt")
