"""
Script to visualize a distance matrix as a cluster heatmap using seaborn. 
https://seaborn.pydata.org/generated/seaborn.clustermap.html
"""


# === Imports === 

import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt
from os.path import join
import os
import itertools
import numpy as np


# === Parameters === 

corpus = "ELTeC-eng" # name of the current test corpus / subfolder
target = "Nesbit_ENG18890" # filename of the target text: Malot_FRA02603|Gardonyi_HU06982|Nesbit_ENG18890|Trollope_ENG18400|Omar
numtexts = 25 # Number of texts to sample for density plot
workdir = join(os.path.realpath(os.path.dirname(__file__)))

matrixfile = join(workdir, corpus, "distance_table_1500mfw_0c.txt")

clustermapfile = join(workdir, corpus, "distance-matrix-clustermap2.png")
ranksfile = join(workdir, corpus, "ranked_candidates.csv")
statsfile = join(workdir, corpus, "distances-statistics.csv")
densityfile1 = join(workdir, corpus, "distances_density_1-same-diff.png")
densityfile2 = join(workdir, corpus, "distances_density_2-target-same.png")
densityfile3 = join(workdir, corpus, "distances_density_3.target-same-diff.png")



# === Functions === 

def read_matrix(): 
    with open(matrixfile, "r", encoding="utf8") as infile: 
        matrix = pd.read_csv(infile, sep=" ",    index_col=0)
    print(matrix.head())
    return matrix


def rank_candidates(matrix): 
    print("rank_candidates")
    ranked_candidates = matrix.loc[target].sort_values(ascending=True).round(4)
    print(ranked_candidates[0:10])
    with open(ranksfile, "w", encoding="utf8") as outfile: 
        ranked_candidates.to_csv(outfile)



def make_clustermap(matrix): 
    print(matrix.head())
    sns.set_style("whitegrid")
    plt.figure()
    title = "Cluster heatmap of stylometric distances: " + str(target)
    sns.clustermap(
        data=matrix,
        method="ward", # "ward" (paired!) | "centroid" etc.
        robust=True,
        figsize=(18,18),
        annot=True,
        square=True,
        linewidths=0.8,
        linecolor="white",
        cmap="hot",
        cbar=True,    
        )
    plt.tight_layout()
    plt.savefig(clustermapfile, dpi=300)
    plt.close()


def select_distances(matrix): 
    books = list(matrix.index)

    # Get all pairs
    all_pairs = [item for item in itertools.permutations(books, 2)]
    # Select pairs with "target" in first position (these are unique and do not include pairs between different target texts!!)
    target_pairs = [pair for pair in all_pairs if pair[0] == target and target[0:6] != pair[1][0:6]]
    # Select pairs with same author, excluding any pairs involving the target author (these include reverse duplicates)
    sameauthor_pairs = [pair for pair in all_pairs if pair[0][0:6] == pair[1][0:6] and target[0:6] != pair[0][0:6] and target[0:6] != pair[1][0:6]]
    # Select pairs with different authors, excluding any pairs involving the target (these include reverse duplicates)
    diffauthor_pairs = [pair for pair in all_pairs if pair[0][0:6] != pair[1][0:6] and target[0:6] != pair[0][0:6] and target[0:6] != pair[1][0:6]]

    # Collect the different groups of distances; shape into df
    distances = []
    for item in target_pairs: 
        distances.append((target, matrix.loc[item[0],item[1]]))
    for item in sameauthor_pairs: 
        distances.append(("within-author", matrix.loc[item[0],item[1]]))
    for item in diffauthor_pairs: 
        distances.append(("between-author", matrix.loc[item[0],item[1]]))
    distances = pd.DataFrame(distances, columns=["type", "distance"])
    #print(distances.head())

    # Save the the typical values for inspection
    stats = []
    for cat in [target, "between-author", "within-author"]: 
        stats.append(pd.Series(distances.query("type in @cat").describe()["distance"], name=cat))
    statistics = pd.DataFrame(stats).T
    print(statistics)
    with open(statsfile, "w", encoding="utf8") as outfile: 
        statistics.to_csv(outfile, sep=";")   
    return distances




def make_densityplot1(distances): 
    include = ["within-author", "between-author"]
    distances = distances.query("type in @include")
    distances_sampled = distances.groupby(by="type").sample(n=numtexts)
    sns.set_style("whitegrid")
    plt.figure()
    title="Comparison of distributions of distances"
    ax = sns.displot(
        data=distances_sampled,
        x="distance",
        hue="type",
        kind="kde", 
        fill=True, 
        rug=True)
    plt.legend()
    sns.move_legend(ax, "center")
    plt.tight_layout()
    plt.savefig(densityfile1, dpi=300)
    plt.close()



def make_densityplot2(distances): 
    include = [target, "within-author"]
    distances = distances.query("type in @include")
    distances_sampled = distances.groupby(by="type").sample(n=numtexts)
    sns.set_style("whitegrid")
    plt.figure()
    title="Comparison of distributions of distances"
    sns.displot(
        data=distances_sampled,
        x="distance",
        hue="type",
        kind="kde", 
        fill=True, 
        rug=True)
    plt.legend()
    plt.tight_layout()
    plt.savefig(densityfile2, dpi=300)



def make_densityplot3(distances): 
    distances_sampled = distances.groupby(by="type").sample(n=numtexts)
    sns.set_style("whitegrid")
    plt.figure()
    title="Comparison of distributions of distances"
    sns.displot(
        data=distances_sampled,
        x="distance",
        hue="type",
        kind="kde", 
        fill=True, 
        rug=True)
    plt.legend()
    plt.tight_layout()
    plt.savefig(densityfile3, dpi=300)




# === Main === 


def main(): 
    matrix = read_matrix()
    rank_candidates(matrix)
    make_clustermap(matrix)
    distances = select_distances(matrix)
    make_densityplot1(distances)
    make_densityplot2(distances)
    make_densityplot3(distances)
main()