# distance-matrix

Companion code to: "Dear fellow stylometrists, let's drop the dendrogram and cherish the distance matrix", _The Dragonfly's Gaze_, 2023, URL: https://dragonfly.hypotheses.org/?p=1414. 